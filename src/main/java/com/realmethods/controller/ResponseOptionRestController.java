/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements Struts action processing for business entity ResponseOption.
 *
 * @author Dev Team
 */
@RestController
@RequestMapping("/ResponseOption")
public class ResponseOptionRestController extends BaseSpringRestController
{

    /**
     * Handles saving a ResponseOption BO.  if not key provided, calls create, otherwise calls save
     * @param		ResponseOption responseOption
     * @return		ResponseOption
     */
	@RequestMapping("/save")
    public ResponseOption save( @RequestBody ResponseOption responseOption )
    {
    	// assign locally
    	this.responseOption = responseOption;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a ResponseOption BO
     * @param		Long responseOptionId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="responseOption.responseOptionId", required=false) Long responseOptionId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ResponseOptionBusinessDelegate delegate = ResponseOptionBusinessDelegate.getResponseOptionInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ResponseOptionPrimaryKey( responseOptionId ) );
        		LOGGER.info( "ResponseOptionController:delete() - successfully deleted ResponseOption with key " + responseOption.getResponseOptionPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ResponseOptionPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ResponseOptionController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ResponseOptionController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a ResponseOption BO
     * @param		Long responseOptionId
     * @return		ResponseOption
     */    
    @RequestMapping("/load")
    public ResponseOption load( @RequestParam(value="responseOption.responseOptionId", required=true) Long responseOptionId )
    {    	
        ResponseOptionPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****ResponseOption.load pk is " + responseOptionId );
        	if ( responseOptionId != null )
        	{
        		pk = new ResponseOptionPrimaryKey( responseOptionId );
        		
        		// load the ResponseOption
	            this.responseOption = ResponseOptionBusinessDelegate.getResponseOptionInstance().getResponseOption( pk );
	            
	            LOGGER.info( "ResponseOptionController:load() - successfully loaded - " + this.responseOption.toString() );             
			}
			else
			{
	            LOGGER.info( "ResponseOptionController:load() - unable to locate the primary key as an attribute or a selection for - " + responseOption.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ResponseOptionController:load() - failed to load ResponseOption using Id " + responseOptionId + ", " + exc.getMessage() );
            return null;
        }

        return responseOption;

    }

    /**
     * Handles loading all ResponseOption business objects
     * @return		List<ResponseOption>
     */
    @RequestMapping("/loadAll")
    public List<ResponseOption> loadAll()
    {                
        List<ResponseOption> responseOptionList = null;
        
    	try
        {                        
            // load the ResponseOption
            responseOptionList = ResponseOptionBusinessDelegate.getResponseOptionInstance().getAllResponseOption();
            
            if ( responseOptionList != null )
                LOGGER.info(  "ResponseOptionController:loadAllResponseOption() - successfully loaded all ResponseOptions" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ResponseOptionController:loadAll() - failed to load all ResponseOptions - " + exc.getMessage() );
        	return null;
            
        }

        return responseOptionList;
                            
    }


// findAllBy methods




    /**
     * Handles creating a ResponseOption BO
     * @return		ResponseOption
     */
    protected ResponseOption create()
    {
        try
        {       
			this.responseOption = ResponseOptionBusinessDelegate.getResponseOptionInstance().createResponseOption( responseOption );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ResponseOptionController:create() - exception ResponseOption - " + exc.getMessage());        	
        	return null;
        }
        
        return this.responseOption;
    }

    /**
     * Handles updating a ResponseOption BO
     * @return		ResponseOption
     */    
    protected ResponseOption update()
    {
    	// store provided data
        ResponseOption tmp = responseOption;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	responseOption.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ResponseOptionBusiness Delegate            
			ResponseOptionBusinessDelegate delegate = ResponseOptionBusinessDelegate.getResponseOptionInstance();
            this.responseOption = delegate.saveResponseOption( responseOption );
            
            if ( this.responseOption != null )
                LOGGER.info( "ResponseOptionController:update() - successfully updated ResponseOption - " + responseOption.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ResponseOptionController:update() - successfully update ResponseOption - " + exc.getMessage());        	
        	return null;
        }
        
        return this.responseOption;
        
    }


    /**
     * Returns true if the responseOption is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( responseOption != null && responseOption.getResponseOptionPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected ResponseOption load()
    {
    	return( load( new Long( responseOption.getResponseOptionPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected ResponseOption responseOption = null;
    private static final Logger LOGGER = Logger.getLogger(ResponseOption.class.getName());
    
}


