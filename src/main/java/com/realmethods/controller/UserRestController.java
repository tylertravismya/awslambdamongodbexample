/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements Struts action processing for business entity User.
 *
 * @author Dev Team
 */
@RestController
@RequestMapping("/User")
public class UserRestController extends BaseSpringRestController
{

    /**
     * Handles saving a User BO.  if not key provided, calls create, otherwise calls save
     * @param		User user
     * @return		User
     */
	@RequestMapping("/save")
    public User save( @RequestBody User user )
    {
    	// assign locally
    	this.user = user;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a User BO
     * @param		Long userId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="user.referrerId", required=false) Long userId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	UserBusinessDelegate delegate = UserBusinessDelegate.getUserInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new UserPrimaryKey( userId ) );
        		LOGGER.info( "UserController:delete() - successfully deleted User with key " + user.getUserPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new UserPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "UserController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "UserController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a User BO
     * @param		Long userId
     * @return		User
     */    
    @RequestMapping("/load")
    public User load( @RequestParam(value="user.referrerId", required=true) Long userId )
    {    	
        UserPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****User.load pk is " + userId );
        	if ( userId != null )
        	{
        		pk = new UserPrimaryKey( userId );
        		
        		// load the User
	            this.user = UserBusinessDelegate.getUserInstance().getUser( pk );
	            
	            LOGGER.info( "UserController:load() - successfully loaded - " + this.user.toString() );             
			}
			else
			{
	            LOGGER.info( "UserController:load() - unable to locate the primary key as an attribute or a selection for - " + user.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "UserController:load() - failed to load User using Id " + userId + ", " + exc.getMessage() );
            return null;
        }

        return user;

    }

    /**
     * Handles loading all User business objects
     * @return		List<User>
     */
    @RequestMapping("/loadAll")
    public List<User> loadAll()
    {                
        List<User> userList = null;
        
    	try
        {                        
            // load the User
            userList = UserBusinessDelegate.getUserInstance().getAllUser();
            
            if ( userList != null )
                LOGGER.info(  "UserController:loadAllUser() - successfully loaded all Users" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "UserController:loadAll() - failed to load all Users - " + exc.getMessage() );
        	return null;
            
        }

        return userList;
                            
    }


// findAllBy methods



    /**
     * save ReferenceProviders on User
     * @param		Long userId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		User
     */     
	@RequestMapping("/saveReferenceProviders")
	public User saveReferenceProviders( @RequestParam(value="user.referrerId", required=false) Long userId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( userId ) == null )
			return( null );
		
		ReferrerPrimaryKey pk 					= null;
		Referrer child							= null;
		ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferrerPrimaryKey( childId );
			
			try
			{
				// find the Referrer
				child = childDelegate.getReferrer( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "UserController:saveReferenceProviders() failed get child Referrer using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the ReferenceProviders 
			user.getReferenceProviders().add( child );				
		}
		else
		{
			// clear out the ReferenceProviders but 
			user.getReferenceProviders().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferrerPrimaryKey( id );
					try
					{
						// find the Referrer
						child = childDelegate.getReferrer( pk );
						// add it to the ReferenceProviders List
						user.getReferenceProviders().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "UserController:saveReferenceProviders() failed get child Referrer using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
		}
		catch( Exception exc )
		{
			LOGGER.info( "UserController:saveReferenceProviders() failed saving parent User - " + exc.getMessage() );
		}

		return user;
	}

    /**
     * delete ReferenceProviders on User
     * @param		Long userId
     * @param		Long[] childIds
     * @return		User
     */     	
	@RequestMapping("/deleteReferenceProviders")
	public User deleteReferenceProviders( @RequestParam(value="user.referrerId", required=true) Long userId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( userId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			ReferrerPrimaryKey pk 					= null;
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<Referrer> children					= user.getReferenceProviders();
			Referrer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferrerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferrer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "UserController:deleteReferenceProviders() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of Referrer back to the user
			user.setReferenceProviders( children );			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "UserController:deleteReferenceProviders() failed to save the User - " + exc.getMessage() );
			}
		}
		
		return user;
	}

    /**
     * save RefererGroups on User
     * @param		Long userId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		User
     */     
	@RequestMapping("/saveRefererGroups")
	public User saveRefererGroups( @RequestParam(value="user.referrerId", required=false) Long userId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( userId ) == null )
			return( null );
		
		ReferrerGroupPrimaryKey pk 					= null;
		ReferrerGroup child							= null;
		ReferrerGroupBusinessDelegate childDelegate 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferrerGroupPrimaryKey( childId );
			
			try
			{
				// find the ReferrerGroup
				child = childDelegate.getReferrerGroup( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "UserController:saveRefererGroups() failed get child ReferrerGroup using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the RefererGroups 
			user.getRefererGroups().add( child );				
		}
		else
		{
			// clear out the RefererGroups but 
			user.getRefererGroups().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferrerGroupPrimaryKey( id );
					try
					{
						// find the ReferrerGroup
						child = childDelegate.getReferrerGroup( pk );
						// add it to the RefererGroups List
						user.getRefererGroups().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "UserController:saveRefererGroups() failed get child ReferrerGroup using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
		}
		catch( Exception exc )
		{
			LOGGER.info( "UserController:saveRefererGroups() failed saving parent User - " + exc.getMessage() );
		}

		return user;
	}

    /**
     * delete RefererGroups on User
     * @param		Long userId
     * @param		Long[] childIds
     * @return		User
     */     	
	@RequestMapping("/deleteRefererGroups")
	public User deleteRefererGroups( @RequestParam(value="user.referrerId", required=true) Long userId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( userId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			ReferrerGroupPrimaryKey pk 					= null;
			ReferrerGroupBusinessDelegate childDelegate 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<ReferrerGroup> children					= user.getRefererGroups();
			ReferrerGroup child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferrerGroupPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferrerGroup( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "UserController:deleteRefererGroups() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of ReferrerGroup back to the user
			user.setRefererGroups( children );			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "UserController:deleteRefererGroups() failed to save the User - " + exc.getMessage() );
			}
		}
		
		return user;
	}

    /**
     * save ReferenceReceivers on User
     * @param		Long userId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		User
     */     
	@RequestMapping("/saveReferenceReceivers")
	public User saveReferenceReceivers( @RequestParam(value="user.referrerId", required=false) Long userId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( userId ) == null )
			return( null );
		
		ReferrerPrimaryKey pk 					= null;
		Referrer child							= null;
		ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferrerPrimaryKey( childId );
			
			try
			{
				// find the Referrer
				child = childDelegate.getReferrer( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "UserController:saveReferenceReceivers() failed get child Referrer using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the ReferenceReceivers 
			user.getReferenceReceivers().add( child );				
		}
		else
		{
			// clear out the ReferenceReceivers but 
			user.getReferenceReceivers().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferrerPrimaryKey( id );
					try
					{
						// find the Referrer
						child = childDelegate.getReferrer( pk );
						// add it to the ReferenceReceivers List
						user.getReferenceReceivers().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "UserController:saveReferenceReceivers() failed get child Referrer using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
		}
		catch( Exception exc )
		{
			LOGGER.info( "UserController:saveReferenceReceivers() failed saving parent User - " + exc.getMessage() );
		}

		return user;
	}

    /**
     * delete ReferenceReceivers on User
     * @param		Long userId
     * @param		Long[] childIds
     * @return		User
     */     	
	@RequestMapping("/deleteReferenceReceivers")
	public User deleteReferenceReceivers( @RequestParam(value="user.referrerId", required=true) Long userId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( userId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			ReferrerPrimaryKey pk 					= null;
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<Referrer> children					= user.getReferenceReceivers();
			Referrer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferrerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferrer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "UserController:deleteReferenceReceivers() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of Referrer back to the user
			user.setReferenceReceivers( children );			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "UserController:deleteReferenceReceivers() failed to save the User - " + exc.getMessage() );
			}
		}
		
		return user;
	}

    /**
     * save ReferenceGroupLinks on User
     * @param		Long userId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		User
     */     
	@RequestMapping("/saveReferenceGroupLinks")
	public User saveReferenceGroupLinks( @RequestParam(value="user.referrerId", required=false) Long userId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( userId ) == null )
			return( null );
		
		ReferenceGroupLinkPrimaryKey pk 					= null;
		ReferenceGroupLink child							= null;
		ReferenceGroupLinkBusinessDelegate childDelegate 	= ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferenceGroupLinkPrimaryKey( childId );
			
			try
			{
				// find the ReferenceGroupLink
				child = childDelegate.getReferenceGroupLink( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "UserController:saveReferenceGroupLinks() failed get child ReferenceGroupLink using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the ReferenceGroupLinks 
			user.getReferenceGroupLinks().add( child );				
		}
		else
		{
			// clear out the ReferenceGroupLinks but 
			user.getReferenceGroupLinks().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferenceGroupLinkPrimaryKey( id );
					try
					{
						// find the ReferenceGroupLink
						child = childDelegate.getReferenceGroupLink( pk );
						// add it to the ReferenceGroupLinks List
						user.getReferenceGroupLinks().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "UserController:saveReferenceGroupLinks() failed get child ReferenceGroupLink using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
		}
		catch( Exception exc )
		{
			LOGGER.info( "UserController:saveReferenceGroupLinks() failed saving parent User - " + exc.getMessage() );
		}

		return user;
	}

    /**
     * delete ReferenceGroupLinks on User
     * @param		Long userId
     * @param		Long[] childIds
     * @return		User
     */     	
	@RequestMapping("/deleteReferenceGroupLinks")
	public User deleteReferenceGroupLinks( @RequestParam(value="user.referrerId", required=true) Long userId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( userId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			ReferenceGroupLinkPrimaryKey pk 					= null;
			ReferenceGroupLinkBusinessDelegate childDelegate 	= ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<ReferenceGroupLink> children					= user.getReferenceGroupLinks();
			ReferenceGroupLink child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferenceGroupLinkPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferenceGroupLink( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "UserController:deleteReferenceGroupLinks() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of ReferenceGroupLink back to the user
			user.setReferenceGroupLinks( children );			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "UserController:deleteReferenceGroupLinks() failed to save the User - " + exc.getMessage() );
			}
		}
		
		return user;
	}


    /**
     * Handles creating a User BO
     * @return		User
     */
    protected User create()
    {
        try
        {       
			this.user = UserBusinessDelegate.getUserInstance().createUser( user );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "UserController:create() - exception User - " + exc.getMessage());        	
        	return null;
        }
        
        return this.user;
    }

    /**
     * Handles updating a User BO
     * @return		User
     */    
    protected User update()
    {
    	// store provided data
        User tmp = user;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	user.copyShallow( tmp );
    	
        try
        {                        	        
			// create the UserBusiness Delegate            
			UserBusinessDelegate delegate = UserBusinessDelegate.getUserInstance();
            this.user = delegate.saveUser( user );
            
            if ( this.user != null )
                LOGGER.info( "UserController:update() - successfully updated User - " + user.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "UserController:update() - successfully update User - " + exc.getMessage());        	
        	return null;
        }
        
        return this.user;
        
    }


    /**
     * Returns true if the user is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( user != null && user.getUserPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected User load()
    {
    	return( load( new Long( user.getUserPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected User user = null;
    private static final Logger LOGGER = Logger.getLogger(User.class.getName());
    
}


