/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.realmethods.exception.*;

    import com.realmethods.primarykey.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity Activity
 * using the Morphia Java Datastore.
 *
 * @author Dev Team
 */
 public class ActivityDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ActivityDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a Activity from the persistent store, using the provided primary key. 
     * If no match is found, a null Activity is returned.
     * <p>
     * @param       pk
     * @return      Activity
     * @exception   ProcessingException
     */
    public Activity findActivity( ActivityPrimaryKey pk ) 
    throws ProcessingException
    {
    	Activity businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ActivityDAO.findActivity cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( Activity.class )
        	       						.field( "activityId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ActivityDAO.findActivity failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new Activity into the persistent store.
     * @param       businessObject
     * @return      newly persisted Activity
     * @exception   ProcessingException
     */
    public Activity createActivity( Activity businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "activityId" ) ;
    		businessObject.setActivityId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ActivityDAO.createActivity created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ActivityDAO.createActivity - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided Activity to the persistent store.
     *
     * @param       businessObject
     * @return      Activity	stored entity
     * @exception   ProcessingException 
     */
    public Activity saveActivity( Activity businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ActivityDAO:saveActivity - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a Activity from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteActivity( ActivityPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findActivity( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ActivityDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ActivityDAO.deleteActivity failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Activitys
     * @return		ArrayList<Activity>
     * @exception   ProcessingException
     */
    public ArrayList<Activity> findAllActivity()
    throws ProcessingException
    {
		final ArrayList<Activity> list 	= new ArrayList<Activity>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( Activity.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ActivityDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ActivityDAO.findAllActivity failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ActivityDAO:findAllActivitys() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.realmethods.Activity" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "activityId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(Activity.class.getName());
}
