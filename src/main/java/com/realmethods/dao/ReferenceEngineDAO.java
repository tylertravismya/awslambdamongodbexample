/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.realmethods.exception.*;

    import com.realmethods.primarykey.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity ReferenceEngine
 * using the Morphia Java Datastore.
 *
 * @author Dev Team
 */
 public class ReferenceEngineDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ReferenceEngineDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a ReferenceEngine from the persistent store, using the provided primary key. 
     * If no match is found, a null ReferenceEngine is returned.
     * <p>
     * @param       pk
     * @return      ReferenceEngine
     * @exception   ProcessingException
     */
    public ReferenceEngine findReferenceEngine( ReferenceEnginePrimaryKey pk ) 
    throws ProcessingException
    {
    	ReferenceEngine businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ReferenceEngineDAO.findReferenceEngine cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( ReferenceEngine.class )
        	       						.field( "referenceEngineId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceEngineDAO.findReferenceEngine failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new ReferenceEngine into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferenceEngine
     * @exception   ProcessingException
     */
    public ReferenceEngine createReferenceEngine( ReferenceEngine businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "referenceEngineId" ) ;
    		businessObject.setReferenceEngineId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ReferenceEngineDAO.createReferenceEngine created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceEngineDAO.createReferenceEngine - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided ReferenceEngine to the persistent store.
     *
     * @param       businessObject
     * @return      ReferenceEngine	stored entity
     * @exception   ProcessingException 
     */
    public ReferenceEngine saveReferenceEngine( ReferenceEngine businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceEngineDAO:saveReferenceEngine - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a ReferenceEngine from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteReferenceEngine( ReferenceEnginePrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findReferenceEngine( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ReferenceEngineDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ReferenceEngineDAO.deleteReferenceEngine failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all ReferenceEngines
     * @return		ArrayList<ReferenceEngine>
     * @exception   ProcessingException
     */
    public ArrayList<ReferenceEngine> findAllReferenceEngine()
    throws ProcessingException
    {
		final ArrayList<ReferenceEngine> list 	= new ArrayList<ReferenceEngine>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( ReferenceEngine.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ReferenceEngineDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ReferenceEngineDAO.findAllReferenceEngine failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ReferenceEngineDAO:findAllReferenceEngines() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.realmethods.ReferenceEngine" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "referenceEngineId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(ReferenceEngine.class.getName());
}
