/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.realmethods.exception.*;

    import com.realmethods.primarykey.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity ResponseOption
 * using the Morphia Java Datastore.
 *
 * @author Dev Team
 */
 public class ResponseOptionDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ResponseOptionDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a ResponseOption from the persistent store, using the provided primary key. 
     * If no match is found, a null ResponseOption is returned.
     * <p>
     * @param       pk
     * @return      ResponseOption
     * @exception   ProcessingException
     */
    public ResponseOption findResponseOption( ResponseOptionPrimaryKey pk ) 
    throws ProcessingException
    {
    	ResponseOption businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ResponseOptionDAO.findResponseOption cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( ResponseOption.class )
        	       						.field( "responseOptionId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ResponseOptionDAO.findResponseOption failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new ResponseOption into the persistent store.
     * @param       businessObject
     * @return      newly persisted ResponseOption
     * @exception   ProcessingException
     */
    public ResponseOption createResponseOption( ResponseOption businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "responseOptionId" ) ;
    		businessObject.setResponseOptionId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ResponseOptionDAO.createResponseOption created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ResponseOptionDAO.createResponseOption - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided ResponseOption to the persistent store.
     *
     * @param       businessObject
     * @return      ResponseOption	stored entity
     * @exception   ProcessingException 
     */
    public ResponseOption saveResponseOption( ResponseOption businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ResponseOptionDAO:saveResponseOption - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a ResponseOption from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteResponseOption( ResponseOptionPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findResponseOption( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ResponseOptionDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ResponseOptionDAO.deleteResponseOption failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all ResponseOptions
     * @return		ArrayList<ResponseOption>
     * @exception   ProcessingException
     */
    public ArrayList<ResponseOption> findAllResponseOption()
    throws ProcessingException
    {
		final ArrayList<ResponseOption> list 	= new ArrayList<ResponseOption>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( ResponseOption.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ResponseOptionDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ResponseOptionDAO.findAllResponseOption failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ResponseOptionDAO:findAllResponseOptions() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.realmethods.ResponseOption" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "responseOptionId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(ResponseOption.class.getName());
}
