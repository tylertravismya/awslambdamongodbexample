/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.realmethods.primarykey.*;
    import com.realmethods.dao.*;
    import com.realmethods.bo.*;
    
import com.realmethods.exception.CreationException;
import com.realmethods.exception.DeletionException;
import com.realmethods.exception.NotFoundException;
import com.realmethods.exception.SaveException;

/**
 * User AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of User related services in the case of a User business related service failing.</li>
 * <li>Exposes a simpler, uniform User interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill User business related services.</li>
 * </ol>
 * <p>
 * @author Dev Team
 */
@Api(value = "User", description = "RESTful API to interact with User resources.")
@Path("/User")
public class UserAWSLambdaDelegate 
extends ReferrerAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public UserAWSLambdaDelegate() {
	}


    /**
     * Creates the provided User
     * @param		businessObject 	User
	 * @param		context		Context	
     * @return     	User
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a User", notes = "Creates User using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static User createUser( 
    		@ApiParam(value = "User entity to create", required = true) User businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null User provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        UserDAO dao  = getUserDAO();
        
        try
        {
            businessObject = dao.createUser( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "UserAWSLambdaDelegate:createUser() - Unable to create User" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the User via a supplied UserPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	User
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a User", notes = "Gets the User associated with the provided primary key", response = User.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static User getUser( 
    		@ApiParam(value = "User primary key", required = true) UserPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        User businessObject  	= null;                
        UserDAO dao 			= getUserDAO();
            
        try
        {
        	businessObject = dao.findUser( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate User with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided User
    * @param		businessObject		User
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a User", notes = "Saves User using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static User saveUser( 
    		@ApiParam(value = "User entity to save", required = true) User businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null User provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        UserPrimaryKey key = businessObject.getUserPrimaryKey();
                    
        if ( key != null )
        {
            UserDAO dao = getUserDAO();

            try
            {                    
                businessObject = (User)dao.saveUser( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save User" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseUserDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create User due to it having a null UserPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Users
     * @param		context		Context
     * @return 	ArrayList<User> 
     */
    @ApiOperation(value = "Get all User", notes = "Get all User from storage", responseContainer = "ArrayList", response = User.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<User> getAllUser( Context context ) 
    	throws NotFoundException {

        ArrayList<User> array	= null;
        UserDAO dao 			= getUserDAO();
        
        try
        {
            array = dao.findAllUser();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllUser - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseUserDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	UserPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a User", notes = "Deletes the User associated with the provided primary key", response = User.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteUser( 
			@ApiParam(value = "User primary key", required = true) UserPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        UserDAO dao  = getUserDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteUser( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete User using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );
        }
         		
        return;
     }

// role related methods

 
    /**
     * Retrieves the ReferenceProviders on a User
     * @param		parentKey	UserPrimaryKey
     * @param		context		Context
     * @return    	Set<Referrer>
     * @exception	NotFoundException
     */
	public static Set<Referrer> getReferenceProviders( UserPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	User user 	= getUser( parentKey, context );
    	return (user.getReferenceProviders());
    }
    
    /**
     * Add the assigned Referrer into the ReferenceProviders of the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		childKey	ReferrerPrimaryKey
	 * @param		context		Context
     * @return    	User
     * @exception	NotFoundException
     */
	public static User addReferenceProviders( 	UserPrimaryKey parentKey, 
									ReferrerPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		User user 	= getUser( parentKey, context );

		// find the Referrer
		Referrer child = ReferrerAWSLambdaDelegate.getReferrer( childKey, context );
		
		// add it to the ReferenceProviders 
		user.getReferenceProviders().add( child );				
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return ( user );
	}

    /**
     * Saves multiple Referrer entities as the ReferenceProviders to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferrerPrimaryKey> childKeys
     * @return    	User
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public User assignReferenceProviders( UserPrimaryKey parentKey, 
											List<ReferrerPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		User user 	= getUser( parentKey, context );
		
		// clear out the ReferenceProviders 
		user.getReferenceProviders().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			Referrer child = null;
			for( ReferrerPrimaryKey childKey : childKeys )
			{
				// retrieve the Referrer
				child = ReferrerAWSLambdaDelegate.getReferrer( childKey, context );

				// add it to the ReferenceProviders List
				user.getReferenceProviders().add( child );
			}
		}
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return( user );
	}

    /**
     * Delete multiple Referrer entities as the ReferenceProviders to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferrerPrimaryKey> childKeys
     * @return    	User
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public User deleteReferenceProviders( UserPrimaryKey parentKey, 
											List<ReferrerPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		User user 	= getUser( parentKey, context );

		if ( childKeys != null )
		{
			Set<Referrer> children	= user.getReferenceProviders();
			Referrer child 			= null;
			
			for( ReferrerPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = ReferrerAWSLambdaDelegate.getReferrer( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					ReferrerAWSLambdaDelegate.deleteReferrer( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of Referrer back to the user
			user.setReferenceProviders( children );			
			// save it 
			user = UserAWSLambdaDelegate.saveUser( user, context );
		}
		
		return ( user );
	}

    /**
     * Retrieves the RefererGroups on a User
     * @param		parentKey	UserPrimaryKey
     * @param		context		Context
     * @return    	Set<ReferrerGroup>
     * @exception	NotFoundException
     */
	public static Set<ReferrerGroup> getRefererGroups( UserPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	User user 	= getUser( parentKey, context );
    	return (user.getRefererGroups());
    }
    
    /**
     * Add the assigned ReferrerGroup into the RefererGroups of the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		childKey	ReferrerGroupPrimaryKey
	 * @param		context		Context
     * @return    	User
     * @exception	NotFoundException
     */
	public static User addRefererGroups( 	UserPrimaryKey parentKey, 
									ReferrerGroupPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		User user 	= getUser( parentKey, context );

		// find the ReferrerGroup
		ReferrerGroup child = ReferrerGroupAWSLambdaDelegate.getReferrerGroup( childKey, context );
		
		// add it to the RefererGroups 
		user.getRefererGroups().add( child );				
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return ( user );
	}

    /**
     * Saves multiple ReferrerGroup entities as the RefererGroups to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferrerGroupPrimaryKey> childKeys
     * @return    	User
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public User assignRefererGroups( UserPrimaryKey parentKey, 
											List<ReferrerGroupPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		User user 	= getUser( parentKey, context );
		
		// clear out the RefererGroups 
		user.getRefererGroups().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			ReferrerGroup child = null;
			for( ReferrerGroupPrimaryKey childKey : childKeys )
			{
				// retrieve the ReferrerGroup
				child = ReferrerGroupAWSLambdaDelegate.getReferrerGroup( childKey, context );

				// add it to the RefererGroups List
				user.getRefererGroups().add( child );
			}
		}
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return( user );
	}

    /**
     * Delete multiple ReferrerGroup entities as the RefererGroups to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferrerGroupPrimaryKey> childKeys
     * @return    	User
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public User deleteRefererGroups( UserPrimaryKey parentKey, 
											List<ReferrerGroupPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		User user 	= getUser( parentKey, context );

		if ( childKeys != null )
		{
			Set<ReferrerGroup> children	= user.getRefererGroups();
			ReferrerGroup child 			= null;
			
			for( ReferrerGroupPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = ReferrerGroupAWSLambdaDelegate.getReferrerGroup( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					ReferrerGroupAWSLambdaDelegate.deleteReferrerGroup( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of ReferrerGroup back to the user
			user.setRefererGroups( children );			
			// save it 
			user = UserAWSLambdaDelegate.saveUser( user, context );
		}
		
		return ( user );
	}

    /**
     * Retrieves the ReferenceReceivers on a User
     * @param		parentKey	UserPrimaryKey
     * @param		context		Context
     * @return    	Set<Referrer>
     * @exception	NotFoundException
     */
	public static Set<Referrer> getReferenceReceivers( UserPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	User user 	= getUser( parentKey, context );
    	return (user.getReferenceReceivers());
    }
    
    /**
     * Add the assigned Referrer into the ReferenceReceivers of the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		childKey	ReferrerPrimaryKey
	 * @param		context		Context
     * @return    	User
     * @exception	NotFoundException
     */
	public static User addReferenceReceivers( 	UserPrimaryKey parentKey, 
									ReferrerPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		User user 	= getUser( parentKey, context );

		// find the Referrer
		Referrer child = ReferrerAWSLambdaDelegate.getReferrer( childKey, context );
		
		// add it to the ReferenceReceivers 
		user.getReferenceReceivers().add( child );				
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return ( user );
	}

    /**
     * Saves multiple Referrer entities as the ReferenceReceivers to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferrerPrimaryKey> childKeys
     * @return    	User
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public User assignReferenceReceivers( UserPrimaryKey parentKey, 
											List<ReferrerPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		User user 	= getUser( parentKey, context );
		
		// clear out the ReferenceReceivers 
		user.getReferenceReceivers().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			Referrer child = null;
			for( ReferrerPrimaryKey childKey : childKeys )
			{
				// retrieve the Referrer
				child = ReferrerAWSLambdaDelegate.getReferrer( childKey, context );

				// add it to the ReferenceReceivers List
				user.getReferenceReceivers().add( child );
			}
		}
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return( user );
	}

    /**
     * Delete multiple Referrer entities as the ReferenceReceivers to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferrerPrimaryKey> childKeys
     * @return    	User
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public User deleteReferenceReceivers( UserPrimaryKey parentKey, 
											List<ReferrerPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		User user 	= getUser( parentKey, context );

		if ( childKeys != null )
		{
			Set<Referrer> children	= user.getReferenceReceivers();
			Referrer child 			= null;
			
			for( ReferrerPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = ReferrerAWSLambdaDelegate.getReferrer( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					ReferrerAWSLambdaDelegate.deleteReferrer( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of Referrer back to the user
			user.setReferenceReceivers( children );			
			// save it 
			user = UserAWSLambdaDelegate.saveUser( user, context );
		}
		
		return ( user );
	}

    /**
     * Retrieves the ReferenceGroupLinks on a User
     * @param		parentKey	UserPrimaryKey
     * @param		context		Context
     * @return    	Set<ReferenceGroupLink>
     * @exception	NotFoundException
     */
	public static Set<ReferenceGroupLink> getReferenceGroupLinks( UserPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	User user 	= getUser( parentKey, context );
    	return (user.getReferenceGroupLinks());
    }
    
    /**
     * Add the assigned ReferenceGroupLink into the ReferenceGroupLinks of the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		childKey	ReferenceGroupLinkPrimaryKey
	 * @param		context		Context
     * @return    	User
     * @exception	NotFoundException
     */
	public static User addReferenceGroupLinks( 	UserPrimaryKey parentKey, 
									ReferenceGroupLinkPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		User user 	= getUser( parentKey, context );

		// find the ReferenceGroupLink
		ReferenceGroupLink child = ReferenceGroupLinkAWSLambdaDelegate.getReferenceGroupLink( childKey, context );
		
		// add it to the ReferenceGroupLinks 
		user.getReferenceGroupLinks().add( child );				
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return ( user );
	}

    /**
     * Saves multiple ReferenceGroupLink entities as the ReferenceGroupLinks to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferenceGroupLinkPrimaryKey> childKeys
     * @return    	User
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public User assignReferenceGroupLinks( UserPrimaryKey parentKey, 
											List<ReferenceGroupLinkPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		User user 	= getUser( parentKey, context );
		
		// clear out the ReferenceGroupLinks 
		user.getReferenceGroupLinks().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			ReferenceGroupLink child = null;
			for( ReferenceGroupLinkPrimaryKey childKey : childKeys )
			{
				// retrieve the ReferenceGroupLink
				child = ReferenceGroupLinkAWSLambdaDelegate.getReferenceGroupLink( childKey, context );

				// add it to the ReferenceGroupLinks List
				user.getReferenceGroupLinks().add( child );
			}
		}
		
		// save the User
		user = UserAWSLambdaDelegate.saveUser( user, context );

		return( user );
	}

    /**
     * Delete multiple ReferenceGroupLink entities as the ReferenceGroupLinks to the relevant User
     * @param		parentKey	UserPrimaryKey
     * @param		List<ReferenceGroupLinkPrimaryKey> childKeys
     * @return    	User
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public User deleteReferenceGroupLinks( UserPrimaryKey parentKey, 
											List<ReferenceGroupLinkPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		User user 	= getUser( parentKey, context );

		if ( childKeys != null )
		{
			Set<ReferenceGroupLink> children	= user.getReferenceGroupLinks();
			ReferenceGroupLink child 			= null;
			
			for( ReferenceGroupLinkPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = ReferenceGroupLinkAWSLambdaDelegate.getReferenceGroupLink( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					ReferenceGroupLinkAWSLambdaDelegate.deleteReferenceGroupLink( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of ReferenceGroupLink back to the user
			user.setReferenceGroupLinks( children );			
			// save it 
			user = UserAWSLambdaDelegate.saveUser( user, context );
		}
		
		return ( user );
	}

 	
    /**
     * Returns the User specific DAO.
     *
     * @return      User DAO
     */
    public static UserDAO getUserDAO()
    {
        return( new com.realmethods.dao.UserDAO() ); 
    }

    /**
     * Release the UserDAO back to the FrameworkDAOFactory
     */
    public static void releaseUserDAO( com.realmethods.dao.UserDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(UserAWSLambdaDelegate.class.getName());
}

