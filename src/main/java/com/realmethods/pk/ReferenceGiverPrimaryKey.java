/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.primarykey;

import java.util.*;

/**
 * ReferenceGiver PrimaryKey class.
 * 
 * @author    Dev Team
 */
// AIB : #getPrimaryKeyClassDecl() 
public class ReferenceGiverPrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public ReferenceGiverPrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    referenceGiverId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public ReferenceGiverPrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object referenceGiverId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.referenceGiverId = referenceGiverId != null ? new Long( referenceGiverId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the referenceGiverId.
	* @return    Long
    */    
	public Long getReferenceGiverId()
	{
		return( this.referenceGiverId );
	}            
	
   /**
	* Assigns the referenceGiverId.
	* @return    Long
    */    
	public void setReferenceGiverId( Long id )
	{
		this.referenceGiverId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( referenceGiverId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( referenceGiverId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE ReferenceGiver class.

// AIB : #getKeyFieldDeclarations()
	public Long referenceGiverId;
// ~AIB 	        

}
