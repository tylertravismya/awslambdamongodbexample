/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.realmethods.bo.Base;
import com.realmethods.bo.*;
import com.realmethods.common.JsonTransformer;
import com.realmethods.delegate.*;
import com.realmethods.primarykey.*;
import com.realmethods.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity QuestionGroup.
 *
 * @author Dev Team
 */

public class QuestionGroupRestService extends BaseRestService
{

	public QuestionGroupRestService()
	{}
	
    /**
     * Handles saving a QuestionGroup BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected QuestionGroup save()
    	throws ProcessingException
    {
		// doing it here helps
		getQuestionGroup();

		LOGGER.info( "QuestionGroup.save() on - " + questionGroup );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the questionGroup is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( questionGroup != null && questionGroup.getQuestionGroupPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a QuestionGroup BO
     * @return		QuestionGroup
     * @exception	ProcessingException
     */    
    protected QuestionGroup update()
    	throws ProcessingException
    {
    	// store provided data
        QuestionGroup tmp = questionGroup;

        // load actual data from storage
        loadHelper( questionGroup.getQuestionGroupPrimaryKey() );
    	
    	// copy provided data into actual data
    	questionGroup.copyShallow( tmp );
    	
        try
        {                        	        
			// create the QuestionGroupBusiness Delegate            
			QuestionGroupBusinessDelegate delegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
            this.questionGroup = delegate.saveQuestionGroup( questionGroup );
            
            if ( this.questionGroup != null )
            	LOGGER.info( "QuestionGroupRestService:update() - successfully updated QuestionGroup - " + questionGroup.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "QuestionGroupRestService:update() - successfully update QuestionGroup - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.questionGroup;
        
    }

    /**
     * Handles creating a QuestionGroup BO
     * @return		QuestionGroup
     */
    protected QuestionGroup create()
    	throws ProcessingException
    {
        try
        {       
        	questionGroup 		= getQuestionGroup();
			this.questionGroup 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance().createQuestionGroup( questionGroup );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "QuestionGroupRestService:create() - exception QuestionGroup - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.questionGroup;
    }


    
    /**
     * Handles deleting a QuestionGroup BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	QuestionGroupBusinessDelegate delegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long questionGroupId  = parseId( "questionGroupId" );
        		delegate.delete( new QuestionGroupPrimaryKey( questionGroupId  ) );
        		LOGGER.info( "QuestionGroupRestService:delete() - successfully deleted QuestionGroup with key " + questionGroup.getQuestionGroupPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new QuestionGroupPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "QuestionGroupRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "QuestionGroupRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a QuestionGroup BO
     * @param		Long questionGroupId
     * @exception	ProcessingException
     * @return		QuestionGroup
     */    
    protected QuestionGroup load() 
    	throws ProcessingException
    {    	
        QuestionGroupPrimaryKey pk 	= null;
		Long questionGroupId  = parseId( "questionGroupId" );

    	try
        {
    		LOGGER.info( "QuestionGroup.load pk is " + questionGroupId );
    		
        	if ( questionGroupId != null )
        	{
        		pk = new QuestionGroupPrimaryKey( questionGroupId );

        		loadHelper( pk );

        		// load the contained instance of QuestionGroup
	            this.questionGroup = QuestionGroupBusinessDelegate.getQuestionGroupInstance().getQuestionGroup( pk );
	            
	            LOGGER.info( "QuestionGroupRestService:load() - successfully loaded - " + this.questionGroup.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "QuestionGroupRestService:load() - unable to locate the primary key as an attribute or a selection for - " + questionGroup.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "QuestionGroupRestService:load() - failed to load QuestionGroup using Id " + questionGroupId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return questionGroup;

    }

    /**
     * Handles loading all QuestionGroup business objects
     * @return		List<QuestionGroup>
     * @exception	ProcessingException
     */
    protected List<QuestionGroup> loadAll()
    	throws ProcessingException
    {                
        List<QuestionGroup> questionGroupList = null;
        
    	try
        {                        
            // load the QuestionGroup
            questionGroupList = QuestionGroupBusinessDelegate.getQuestionGroupInstance().getAllQuestionGroup();
            
            if ( questionGroupList != null )
            	LOGGER.info(  "QuestionGroupRestService:loadAllQuestionGroup() - successfully loaded all QuestionGroups" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "QuestionGroupRestService:loadAll() - failed to load all QuestionGroups - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return questionGroupList;
                            
    }




    /**
     * save Questions on QuestionGroup
     * @param		Long questionGroupId
     * @param		Long childId
     * @param		String[] childIds
     * @return		QuestionGroup
     * @exception	ProcessingException
     */     
	protected QuestionGroup saveQuestions()
		throws ProcessingException
	{
		Long questionGroupId = parseId( "questionGroupId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new QuestionGroupPrimaryKey( questionGroupId ) ) == null )
			throw new ProcessingException( "QuestionGroup.saveQuestions() - failed to load parent using Id " + questionGroupId );
		 
		QuestionPrimaryKey pk 					= null;
		Question child							= null;
		List<Question> childList				= null;
		QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
		QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new QuestionPrimaryKey( childId );
			
			try
			{
				// find the Question
				child = childDelegate.getQuestion( pk );
				LOGGER.info( "LeagueRestService:saveQuestionGroup() - found Question" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "QuestionGroupRestService:saveQuestions() failed get child Question using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the Questions, check for null
			if ( questionGroup.getQuestions() != null )
				questionGroup.getQuestions().add( child );

			LOGGER.info( "LeagueRestService:saveQuestionGroup() - added Question to parent" );

		}
		else
		{
			// clear or create the Questions
			if ( questionGroup.getQuestions() != null )
				questionGroup.getQuestions().clear();
			else
				questionGroup.setQuestions( new HashSet<Question>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new QuestionPrimaryKey( id );
					try
					{
						// find the Question
						child = childDelegate.getQuestion( pk );
						// add it to the Questions List
						questionGroup.getQuestions().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "QuestionGroupRestService:saveQuestions() failed get child Question using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the QuestionGroup
			parentDelegate.saveQuestionGroup( questionGroup );
			LOGGER.info( "LeagueRestService:saveQuestionGroup() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "QuestionGroupRestService:saveQuestions() failed saving parent QuestionGroup - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return questionGroup;
	}

    /**
     * delete Questions on QuestionGroup
     * @return		QuestionGroup
     * @exception	ProcessingException
     */     	
	protected QuestionGroup deleteQuestions()
		throws ProcessingException
	{		
		Long questionGroupId = parseId( "questionGroupId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new QuestionGroupPrimaryKey( questionGroupId ) ) == null )
			throw new ProcessingException( "QuestionGroup.deleteQuestions() - failed to load using Id " + questionGroupId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			QuestionPrimaryKey pk 					= null;
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			Set<Question> children					= questionGroup.getQuestions();
			Question child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new QuestionPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getQuestion( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "QuestionGroupRestService:deleteQuestions() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of Question back to the questionGroup
			questionGroup.setQuestions( children );
			
			// save it 
			try
			{
				questionGroup = parentDelegate.saveQuestionGroup( questionGroup );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "QuestionGroupRestService:deleteQuestions() failed to save the QuestionGroup - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return questionGroup;
	}

    /**
     * save QuestionGroups on QuestionGroup
     * @param		Long questionGroupId
     * @param		Long childId
     * @param		String[] childIds
     * @return		QuestionGroup
     * @exception	ProcessingException
     */     
	protected QuestionGroup saveQuestionGroups()
		throws ProcessingException
	{
		Long questionGroupId = parseId( "questionGroupId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new QuestionGroupPrimaryKey( questionGroupId ) ) == null )
			throw new ProcessingException( "QuestionGroup.saveQuestionGroups() - failed to load parent using Id " + questionGroupId );
		 
		QuestionGroupPrimaryKey pk 					= null;
		QuestionGroup child							= null;
		List<QuestionGroup> childList				= null;
		QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
		QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new QuestionGroupPrimaryKey( childId );
			
			try
			{
				// find the QuestionGroup
				child = childDelegate.getQuestionGroup( pk );
				LOGGER.info( "LeagueRestService:saveQuestionGroup() - found QuestionGroup" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "QuestionGroupRestService:saveQuestionGroups() failed get child QuestionGroup using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the QuestionGroups, check for null
			if ( questionGroup.getQuestionGroups() != null )
				questionGroup.getQuestionGroups().add( child );

			LOGGER.info( "LeagueRestService:saveQuestionGroup() - added QuestionGroup to parent" );

		}
		else
		{
			// clear or create the QuestionGroups
			if ( questionGroup.getQuestionGroups() != null )
				questionGroup.getQuestionGroups().clear();
			else
				questionGroup.setQuestionGroups( new HashSet<QuestionGroup>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new QuestionGroupPrimaryKey( id );
					try
					{
						// find the QuestionGroup
						child = childDelegate.getQuestionGroup( pk );
						// add it to the QuestionGroups List
						questionGroup.getQuestionGroups().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "QuestionGroupRestService:saveQuestionGroups() failed get child QuestionGroup using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the QuestionGroup
			parentDelegate.saveQuestionGroup( questionGroup );
			LOGGER.info( "LeagueRestService:saveQuestionGroup() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "QuestionGroupRestService:saveQuestionGroups() failed saving parent QuestionGroup - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return questionGroup;
	}

    /**
     * delete QuestionGroups on QuestionGroup
     * @return		QuestionGroup
     * @exception	ProcessingException
     */     	
	protected QuestionGroup deleteQuestionGroups()
		throws ProcessingException
	{		
		Long questionGroupId = parseId( "questionGroupId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new QuestionGroupPrimaryKey( questionGroupId ) ) == null )
			throw new ProcessingException( "QuestionGroup.deleteQuestionGroups() - failed to load using Id " + questionGroupId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			QuestionGroupPrimaryKey pk 					= null;
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			Set<QuestionGroup> children					= questionGroup.getQuestionGroups();
			QuestionGroup child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new QuestionGroupPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getQuestionGroup( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "QuestionGroupRestService:deleteQuestionGroups() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of QuestionGroup back to the questionGroup
			questionGroup.setQuestionGroups( children );
			
			// save it 
			try
			{
				questionGroup = parentDelegate.saveQuestionGroup( questionGroup );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "QuestionGroupRestService:deleteQuestionGroups() failed to save the QuestionGroup - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return questionGroup;
	}



    protected QuestionGroup loadHelper( QuestionGroupPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "QuestionGroup.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of QuestionGroup
	            this.questionGroup = QuestionGroupBusinessDelegate.getQuestionGroupInstance().getQuestionGroup( pk );
	            
	            LOGGER.info( "QuestionGroupRestService:loadHelper() - successfully loaded - " + this.questionGroup.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "QuestionGroupRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "QuestionGroupRestService:load() - failed to load QuestionGroup using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return questionGroup;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveQuestions":
	        	returnVal = saveQuestions().getQuestions();
	        	break;
	        case "deleteQuestions":
	        	returnVal = deleteQuestions().getQuestions();
	        	break;
	        case "loadQuestions" :
	        	returnVal = load().getQuestions();
	        	break;
	        case "saveQuestionGroups":
	        	returnVal = saveQuestionGroups().getQuestionGroups();
	        	break;
	        case "deleteQuestionGroups":
	        	returnVal = deleteQuestionGroups().getQuestionGroups();
	        	break;
	        case "loadQuestionGroups" :
	        	returnVal = load().getQuestionGroups();
	        	break;
          default:
        	signalBadRequest();
            throw new ProcessingException("QuestionGroup.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a QuestionGroup. Found in the request body.
	 * 
	 * @return QuestionGroup
	 */
	private QuestionGroup getQuestionGroup()
	{
		if ( questionGroup == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				questionGroup = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), QuestionGroup.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "QuestionGroupRestService.getQuestionGroup() - failed to Json map from String to QuestionGroup - " + exc.getMessage() );
	        }			
		}
		return( questionGroup );
	}
	
	protected String getSubclassName()
	{ return( "QuestionGroupRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private QuestionGroup questionGroup 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
