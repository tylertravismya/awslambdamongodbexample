/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.realmethods.bo.Base;
import com.realmethods.bo.*;
import com.realmethods.common.JsonTransformer;
import com.realmethods.delegate.*;
import com.realmethods.primarykey.*;
import com.realmethods.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity ReferenceGiver.
 *
 * @author Dev Team
 */

public class ReferenceGiverRestService extends BaseRestService
{

	public ReferenceGiverRestService()
	{}
	
    /**
     * Handles saving a ReferenceGiver BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected ReferenceGiver save()
    	throws ProcessingException
    {
		// doing it here helps
		getReferenceGiver();

		LOGGER.info( "ReferenceGiver.save() on - " + referenceGiver );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the referenceGiver is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referenceGiver != null && referenceGiver.getReferenceGiverPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a ReferenceGiver BO
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */    
    protected ReferenceGiver update()
    	throws ProcessingException
    {
    	// store provided data
        ReferenceGiver tmp = referenceGiver;

        // load actual data from storage
        loadHelper( referenceGiver.getReferenceGiverPrimaryKey() );
    	
    	// copy provided data into actual data
    	referenceGiver.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferenceGiverBusiness Delegate            
			ReferenceGiverBusinessDelegate delegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();
            this.referenceGiver = delegate.saveReferenceGiver( referenceGiver );
            
            if ( this.referenceGiver != null )
            	LOGGER.info( "ReferenceGiverRestService:update() - successfully updated ReferenceGiver - " + referenceGiver.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferenceGiverRestService:update() - successfully update ReferenceGiver - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referenceGiver;
        
    }

    /**
     * Handles creating a ReferenceGiver BO
     * @return		ReferenceGiver
     */
    protected ReferenceGiver create()
    	throws ProcessingException
    {
        try
        {       
        	referenceGiver 		= getReferenceGiver();
			this.referenceGiver 	= ReferenceGiverBusinessDelegate.getReferenceGiverInstance().createReferenceGiver( referenceGiver );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferenceGiverRestService:create() - exception ReferenceGiver - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referenceGiver;
    }


    
    /**
     * Handles deleting a ReferenceGiver BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	ReferenceGiverBusinessDelegate delegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long referenceGiverId  = parseId( "referenceGiverId" );
        		delegate.delete( new ReferenceGiverPrimaryKey( referenceGiverId  ) );
        		LOGGER.info( "ReferenceGiverRestService:delete() - successfully deleted ReferenceGiver with key " + referenceGiver.getReferenceGiverPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferenceGiverPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "ReferenceGiverRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "ReferenceGiverRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a ReferenceGiver BO
     * @param		Long referenceGiverId
     * @exception	ProcessingException
     * @return		ReferenceGiver
     */    
    protected ReferenceGiver load() 
    	throws ProcessingException
    {    	
        ReferenceGiverPrimaryKey pk 	= null;
		Long referenceGiverId  = parseId( "referenceGiverId" );

    	try
        {
    		LOGGER.info( "ReferenceGiver.load pk is " + referenceGiverId );
    		
        	if ( referenceGiverId != null )
        	{
        		pk = new ReferenceGiverPrimaryKey( referenceGiverId );

        		loadHelper( pk );

        		// load the contained instance of ReferenceGiver
	            this.referenceGiver = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getReferenceGiver( pk );
	            
	            LOGGER.info( "ReferenceGiverRestService:load() - successfully loaded - " + this.referenceGiver.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:load() - unable to locate the primary key as an attribute or a selection for - " + referenceGiver.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferenceGiverRestService:load() - failed to load ReferenceGiver using Id " + referenceGiverId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referenceGiver;

    }

    /**
     * Handles loading all ReferenceGiver business objects
     * @return		List<ReferenceGiver>
     * @exception	ProcessingException
     */
    protected List<ReferenceGiver> loadAll()
    	throws ProcessingException
    {                
        List<ReferenceGiver> referenceGiverList = null;
        
    	try
        {                        
            // load the ReferenceGiver
            referenceGiverList = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getAllReferenceGiver();
            
            if ( referenceGiverList != null )
            	LOGGER.info(  "ReferenceGiverRestService:loadAllReferenceGiver() - successfully loaded all ReferenceGivers" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferenceGiverRestService:loadAll() - failed to load all ReferenceGivers - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return referenceGiverList;
                            
    }



    /**
     * save QuestionGroup on ReferenceGiver
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver saveQuestionGroup()
		throws ProcessingException
	{
		Long referenceGiverId 	= parseId( "referenceGiverId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			QuestionGroup child 							= null;

			try
			{
				child = childDelegate.getQuestionGroup( new QuestionGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ReferenceGiverRestService:saveQuestionGroup() failed to get QuestionGroup using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			referenceGiver.setQuestionGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:saveQuestionGroup() failed saving parent ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete QuestionGroup on ReferenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver deleteQuestionGroup()
		throws ProcessingException
	{
		Long referenceGiverId = parseId( "referenceGiverId" );
		
 		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );

		if ( referenceGiver.getQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = referenceGiver.getQuestionGroup().getQuestionGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setQuestionGroup( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteQuestionGroup() failed to save ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteQuestionGroup() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}
	
    /**
     * save User on ReferenceGiver
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver saveUser()
		throws ProcessingException
	{
		Long referenceGiverId 	= parseId( "referenceGiverId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ReferenceGiverRestService:saveUser() failed to get User using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			referenceGiver.setUser( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:saveUser() failed saving parent ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete User on ReferenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver deleteUser()
		throws ProcessingException
	{
		Long referenceGiverId = parseId( "referenceGiverId" );
		
 		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );

		if ( referenceGiver.getUser() != null )
		{
			UserPrimaryKey pk = referenceGiver.getUser().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setUser( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteUser() failed to save ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteUser() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}
	
    /**
     * save Referrer on ReferenceGiver
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver saveReferrer()
		throws ProcessingException
	{
		Long referenceGiverId 	= parseId( "referenceGiverId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			Referrer child 							= null;

			try
			{
				child = childDelegate.getReferrer( new ReferrerPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ReferenceGiverRestService:saveReferrer() failed to get Referrer using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			referenceGiver.setReferrer( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:saveReferrer() failed saving parent ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete Referrer on ReferenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver deleteReferrer()
		throws ProcessingException
	{
		Long referenceGiverId = parseId( "referenceGiverId" );
		
 		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );

		if ( referenceGiver.getReferrer() != null )
		{
			ReferrerPrimaryKey pk = referenceGiver.getReferrer().getReferrerPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setReferrer( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteReferrer() failed to save ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerBusinessDelegate childDelegate = ReferrerBusinessDelegate.getReferrerInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteReferrer() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}
	
    /**
     * save LastQuestionAnswered on ReferenceGiver
     * @param		ReferenceGiver referenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver saveLastQuestionAnswered()
		throws ProcessingException
	{
		Long referenceGiverId 	= parseId( "referenceGiverId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();			
			Question child 							= null;

			try
			{
				child = childDelegate.getQuestion( new QuestionPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ReferenceGiverRestService:saveLastQuestionAnswered() failed to get Question using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			referenceGiver.setLastQuestionAnswered( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:saveLastQuestionAnswered() failed saving parent ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}

    /**
     * delete LastQuestionAnswered on ReferenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver deleteLastQuestionAnswered()
		throws ProcessingException
	{
		Long referenceGiverId = parseId( "referenceGiverId" );
		
 		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			return( null );

		if ( referenceGiver.getLastQuestionAnswered() != null )
		{
			QuestionPrimaryKey pk = referenceGiver.getLastQuestionAnswered().getQuestionPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGiver.setLastQuestionAnswered( null );
			try
			{
				ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();

				// save it
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteLastQuestionAnswered() failed to save ReferenceGiver - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				QuestionBusinessDelegate childDelegate = QuestionBusinessDelegate.getQuestionInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:deleteLastQuestionAnswered() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}
	

    /**
     * save Answers on ReferenceGiver
     * @param		Long referenceGiverId
     * @param		Long childId
     * @param		String[] childIds
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     
	protected ReferenceGiver saveAnswers()
		throws ProcessingException
	{
		Long referenceGiverId = parseId( "referenceGiverId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			throw new ProcessingException( "ReferenceGiver.saveAnswers() - failed to load parent using Id " + referenceGiverId );
		 
		AnswerPrimaryKey pk 					= null;
		Answer child							= null;
		List<Answer> childList				= null;
		AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
		ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new AnswerPrimaryKey( childId );
			
			try
			{
				// find the Answer
				child = childDelegate.getAnswer( pk );
				LOGGER.info( "LeagueRestService:saveReferenceGiver() - found Answer" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:saveAnswers() failed get child Answer using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the Answers, check for null
			if ( referenceGiver.getAnswers() != null )
				referenceGiver.getAnswers().add( child );

			LOGGER.info( "LeagueRestService:saveReferenceGiver() - added Answer to parent" );

		}
		else
		{
			// clear or create the Answers
			if ( referenceGiver.getAnswers() != null )
				referenceGiver.getAnswers().clear();
			else
				referenceGiver.setAnswers( new HashSet<Answer>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new AnswerPrimaryKey( id );
					try
					{
						// find the Answer
						child = childDelegate.getAnswer( pk );
						// add it to the Answers List
						referenceGiver.getAnswers().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "ReferenceGiverRestService:saveAnswers() failed get child Answer using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the ReferenceGiver
			parentDelegate.saveReferenceGiver( referenceGiver );
			LOGGER.info( "LeagueRestService:saveReferenceGiver() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "ReferenceGiverRestService:saveAnswers() failed saving parent ReferenceGiver - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return referenceGiver;
	}

    /**
     * delete Answers on ReferenceGiver
     * @return		ReferenceGiver
     * @exception	ProcessingException
     */     	
	protected ReferenceGiver deleteAnswers()
		throws ProcessingException
	{		
		Long referenceGiverId = parseId( "referenceGiverId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new ReferenceGiverPrimaryKey( referenceGiverId ) ) == null )
			throw new ProcessingException( "ReferenceGiver.deleteAnswers() - failed to load using Id " + referenceGiverId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			AnswerPrimaryKey pk 					= null;
			AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
			ReferenceGiverBusinessDelegate parentDelegate = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();
			Set<Answer> children					= referenceGiver.getAnswers();
			Answer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new AnswerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getAnswer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "ReferenceGiverRestService:deleteAnswers() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of Answer back to the referenceGiver
			referenceGiver.setAnswers( children );
			
			// save it 
			try
			{
				referenceGiver = parentDelegate.saveReferenceGiver( referenceGiver );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "ReferenceGiverRestService:deleteAnswers() failed to save the ReferenceGiver - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGiver;
	}



    protected ReferenceGiver loadHelper( ReferenceGiverPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "ReferenceGiver.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of ReferenceGiver
	            this.referenceGiver = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getReferenceGiver( pk );
	            
	            LOGGER.info( "ReferenceGiverRestService:loadHelper() - successfully loaded - " + this.referenceGiver.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGiverRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferenceGiverRestService:load() - failed to load ReferenceGiver using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referenceGiver;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveAnswers":
	        	returnVal = saveAnswers().getAnswers();
	        	break;
	        case "deleteAnswers":
	        	returnVal = deleteAnswers().getAnswers();
	        	break;
	        case "loadAnswers" :
	        	returnVal = load().getAnswers();
	        	break;
 	        case "saveQuestionGroup":
	        	returnVal = saveQuestionGroup().getQuestionGroup();
	        	break;
	        case "deleteQuestionGroup":
	        	returnVal = deleteQuestionGroup().getQuestionGroup();
	        	break;
	        case "loadQuestionGroup" :
	        	returnVal = load().getQuestionGroup();
	        	break;
	        case "saveUser":
	        	returnVal = saveUser().getUser();
	        	break;
	        case "deleteUser":
	        	returnVal = deleteUser().getUser();
	        	break;
	        case "loadUser" :
	        	returnVal = load().getUser();
	        	break;
	        case "saveReferrer":
	        	returnVal = saveReferrer().getReferrer();
	        	break;
	        case "deleteReferrer":
	        	returnVal = deleteReferrer().getReferrer();
	        	break;
	        case "loadReferrer" :
	        	returnVal = load().getReferrer();
	        	break;
	        case "saveLastQuestionAnswered":
	        	returnVal = saveLastQuestionAnswered().getLastQuestionAnswered();
	        	break;
	        case "deleteLastQuestionAnswered":
	        	returnVal = deleteLastQuestionAnswered().getLastQuestionAnswered();
	        	break;
	        case "loadLastQuestionAnswered" :
	        	returnVal = load().getLastQuestionAnswered();
	        	break;
         default:
        	signalBadRequest();
            throw new ProcessingException("ReferenceGiver.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a ReferenceGiver. Found in the request body.
	 * 
	 * @return ReferenceGiver
	 */
	private ReferenceGiver getReferenceGiver()
	{
		if ( referenceGiver == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				referenceGiver = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), ReferenceGiver.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "ReferenceGiverRestService.getReferenceGiver() - failed to Json map from String to ReferenceGiver - " + exc.getMessage() );
	        }			
		}
		return( referenceGiver );
	}
	
	protected String getSubclassName()
	{ return( "ReferenceGiverRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private ReferenceGiver referenceGiver 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
