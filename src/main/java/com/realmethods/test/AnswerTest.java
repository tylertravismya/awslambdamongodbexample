/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/**
 * Test Answer class.
 *
 * @author    Dev Team
 */
public class AnswerTest
{

// constructors

    public AnswerTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a Answer, through a AnswerTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on AnswerTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on AnswerTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new Answer.
     *
     * @return    Answer
     */
    public Answer testCreate()
    throws Throwable
    {
        Answer businessObject = null;

    	{
	        LOGGER.info( "AnswerTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a Answer");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a Answer" );
	
	        try 
	        {            
	            businessObject = AnswerBusinessDelegate.getAnswerInstance().createAnswer( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (AnswerPrimaryKey)businessObject.getAnswerPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a Answer with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a Answer.
     *
     * @return    Answer  
     */
    public Answer testRead()
    throws Throwable
    {
        LOGGER.info( "AnswerTest:testRead()" );
        LOGGER.info( "-- Reading a previously created Answer" );

        Answer businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read Answer with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = AnswerBusinessDelegate.getAnswerInstance().getAnswer( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found Answer " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a Answer.
     *
     * @return    Answer
     */
    public Answer testUpdate()
    throws Throwable
    {

        LOGGER.info( "AnswerTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a Answer." );

        StringBuilder msg = new StringBuilder( "Failed to update a Answer : " );        
        Answer businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created Answer." );
            
            // for use later on...
            thePrimaryKey = (AnswerPrimaryKey)businessObject.getAnswerPrimaryKey();
            
            AnswerBusinessDelegate proxy = AnswerBusinessDelegate.getAnswerInstance();            
            businessObject = proxy.saveAnswer( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved Answer - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a Answer.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "AnswerTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created Answer." );
        
        try
        {
            AnswerBusinessDelegate.getAnswerInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted Answer with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete Answer with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all Answers.
     *
     * @return    Collection
     */
    public ArrayList<Answer> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "AnswerTest:testGetAll() - Retrieving Collection of Answers:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all Answer : " );        
        ArrayList<Answer> collection  = null;

        try
        {
            // call the static get method on the AnswerBusinessDelegate
            collection = AnswerBusinessDelegate.getAnswerInstance().getAllAnswer();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            Answer currentBO  = null;            
	            Iterator<Answer> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getAnswerPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public AnswerTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate Answer
     * 
     * @return    Answer
     */    
    protected Answer getNewBO()
    {
        Answer newBO = new Answer();

// AIB : \#defaultBOOutput() 
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected AnswerPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(Answer.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
