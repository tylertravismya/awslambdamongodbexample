/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.util.logging.*;

/**
 * Base class for application Test classes.
 *
 * @author    Dev Team
 */
public class BaseTest
{
	/**
	 * hidden
	 */
	protected BaseTest() {
	}
	
	public static void runTheTest( Handler logHandler ) 
    {
         try {
		    new ReferrerTest().setHandler(logHandler).testCRUD();
		    new ReferenceGiverTest().setHandler(logHandler).testCRUD();
		    new ReferrerGroupTest().setHandler(logHandler).testCRUD();
		    new ReferenceEngineTest().setHandler(logHandler).testCRUD();
		    new QuestionTest().setHandler(logHandler).testCRUD();
		    new ResponseOptionTest().setHandler(logHandler).testCRUD();
		    new QuestionGroupTest().setHandler(logHandler).testCRUD();
		    new AdminTest().setHandler(logHandler).testCRUD();
		    new ActivityTest().setHandler(logHandler).testCRUD();
		    new CommentTest().setHandler(logHandler).testCRUD();
		    new AnswerTest().setHandler(logHandler).testCRUD();
		    new ReferenceGroupLinkTest().setHandler(logHandler).testCRUD();
        } catch( Throwable exc ) {
        	exc.printStackTrace();
        }
    }
	
}
