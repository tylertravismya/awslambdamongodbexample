/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/**
 * Test Referrer class.
 *
 * @author    Dev Team
 */
public class ReferrerTest
{

// constructors

    public ReferrerTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a Referrer, through a ReferrerTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ReferrerTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ReferrerTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new Referrer.
     *
     * @return    Referrer
     */
    public Referrer testCreate()
    throws Throwable
    {
        Referrer businessObject = null;

    	{
	        LOGGER.info( "ReferrerTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a Referrer");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a Referrer" );
	
	        try 
	        {            
	            businessObject = ReferrerBusinessDelegate.getReferrerInstance().createReferrer( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ReferrerPrimaryKey)businessObject.getReferrerPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a Referrer with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a Referrer.
     *
     * @return    Referrer  
     */
    public Referrer testRead()
    throws Throwable
    {
        LOGGER.info( "ReferrerTest:testRead()" );
        LOGGER.info( "-- Reading a previously created Referrer" );

        Referrer businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read Referrer with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ReferrerBusinessDelegate.getReferrerInstance().getReferrer( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found Referrer " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a Referrer.
     *
     * @return    Referrer
     */
    public Referrer testUpdate()
    throws Throwable
    {

        LOGGER.info( "ReferrerTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a Referrer." );

        StringBuilder msg = new StringBuilder( "Failed to update a Referrer : " );        
        Referrer businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created Referrer." );
            
            // for use later on...
            thePrimaryKey = (ReferrerPrimaryKey)businessObject.getReferrerPrimaryKey();
            
            ReferrerBusinessDelegate proxy = ReferrerBusinessDelegate.getReferrerInstance();            
            businessObject = proxy.saveReferrer( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved Referrer - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a Referrer.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ReferrerTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created Referrer." );
        
        try
        {
            ReferrerBusinessDelegate.getReferrerInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted Referrer with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete Referrer with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all Referrers.
     *
     * @return    Collection
     */
    public ArrayList<Referrer> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ReferrerTest:testGetAll() - Retrieving Collection of Referrers:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all Referrer : " );        
        ArrayList<Referrer> collection  = null;

        try
        {
            // call the static get method on the ReferrerBusinessDelegate
            collection = ReferrerBusinessDelegate.getReferrerInstance().getAllReferrer();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            Referrer currentBO  = null;            
	            Iterator<Referrer> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getReferrerPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ReferrerTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate Referrer
     * 
     * @return    Referrer
     */    
    protected Referrer getNewBO()
    {
        Referrer newBO = new Referrer();

// AIB : \#defaultBOOutput() 
newBO.setFirstName(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setLastName(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setEmailAddress(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setActive(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ReferrerPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(Referrer.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
