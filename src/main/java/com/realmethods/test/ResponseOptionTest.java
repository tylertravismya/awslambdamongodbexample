/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/**
 * Test ResponseOption class.
 *
 * @author    Dev Team
 */
public class ResponseOptionTest
{

// constructors

    public ResponseOptionTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a ResponseOption, through a ResponseOptionTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ResponseOptionTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ResponseOptionTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new ResponseOption.
     *
     * @return    ResponseOption
     */
    public ResponseOption testCreate()
    throws Throwable
    {
        ResponseOption businessObject = null;

    	{
	        LOGGER.info( "ResponseOptionTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a ResponseOption");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a ResponseOption" );
	
	        try 
	        {            
	            businessObject = ResponseOptionBusinessDelegate.getResponseOptionInstance().createResponseOption( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ResponseOptionPrimaryKey)businessObject.getResponseOptionPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a ResponseOption with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a ResponseOption.
     *
     * @return    ResponseOption  
     */
    public ResponseOption testRead()
    throws Throwable
    {
        LOGGER.info( "ResponseOptionTest:testRead()" );
        LOGGER.info( "-- Reading a previously created ResponseOption" );

        ResponseOption businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read ResponseOption with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ResponseOptionBusinessDelegate.getResponseOptionInstance().getResponseOption( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found ResponseOption " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a ResponseOption.
     *
     * @return    ResponseOption
     */
    public ResponseOption testUpdate()
    throws Throwable
    {

        LOGGER.info( "ResponseOptionTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a ResponseOption." );

        StringBuilder msg = new StringBuilder( "Failed to update a ResponseOption : " );        
        ResponseOption businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created ResponseOption." );
            
            // for use later on...
            thePrimaryKey = (ResponseOptionPrimaryKey)businessObject.getResponseOptionPrimaryKey();
            
            ResponseOptionBusinessDelegate proxy = ResponseOptionBusinessDelegate.getResponseOptionInstance();            
            businessObject = proxy.saveResponseOption( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved ResponseOption - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a ResponseOption.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ResponseOptionTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created ResponseOption." );
        
        try
        {
            ResponseOptionBusinessDelegate.getResponseOptionInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted ResponseOption with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete ResponseOption with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all ResponseOptions.
     *
     * @return    Collection
     */
    public ArrayList<ResponseOption> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ResponseOptionTest:testGetAll() - Retrieving Collection of ResponseOptions:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all ResponseOption : " );        
        ArrayList<ResponseOption> collection  = null;

        try
        {
            // call the static get method on the ResponseOptionBusinessDelegate
            collection = ResponseOptionBusinessDelegate.getResponseOptionInstance().getAllResponseOption();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            ResponseOption currentBO  = null;            
	            Iterator<ResponseOption> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getResponseOptionPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ResponseOptionTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate ResponseOption
     * 
     * @return    ResponseOption
     */    
    protected ResponseOption getNewBO()
    {
        ResponseOption newBO = new ResponseOption();

// AIB : \#defaultBOOutput() 
newBO.setResponseText(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setGotoQuestionId(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ResponseOptionPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(ResponseOption.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
