# Specify the provider and access details
provider "aws" {
  region     = "us-east-1"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
  version = "~> 2.0"
}

locals {
  public_key_filename  = "${path.root}/keys/id_rsa.pub"
  private_key_filename = "${path.root}/keys/id_rsa"
}

# Generate an RSA key to be used
resource "tls_private_key" "generated" {
  algorithm = "RSA"
}

# Generate the local SSH Key pair in the directory specified
resource "local_file" "public_key_openssh" {
  content  = "${tls_private_key.generated.public_key_openssh}"
  filename = "${local.public_key_filename}"
}
resource "local_file" "private_key_pem" {
  content  = "${tls_private_key.generated.private_key_pem}"
  filename = "${local.private_key_filename}"
}

resource "aws_key_pair" "generated" {
  key_name   = "pjsk-sshtest-${uuid()}"
  public_key = "${tls_private_key.generated.public_key_openssh}"

  lifecycle {
    ignore_changes = ["key_name"]
  }
}


# Our default security group to for the database
resource "aws_security_group" "mongo" {
  description = "security group created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # mongodb access from anywhere
  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "mongodb" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
    private_key = "${tls_private_key.generated.private_key_pem}"
  }

  instance_type = "t2.micro"
  
  tags = { Name = "mongodb instance" } 

  # standard realmethods community image with mongo started on the default port 
  ami = "ami-0e2a167cf2e0ce6c0"

  # The name of the SSH keypair you've created and downloaded
  # from the AWS console.
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:
  #
  # key_name = "${aib.getParam("terraform.aws-key-pair-name")}"
  key_name = "${aws_key_pair.generated.key_name}"
  
  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.mongo.id}"]
  
  # To ensure ssh access works
    provisioner "remote-exec" {
    inline = [
      "sudo ls",
    ]
  }
}

resource "aws_lambda_function" "addReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::addComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"

  vpc_config {
     subnet_ids = []
     security_group_ids = []
  }  
}

resource "aws_lambda_function" "assignReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::assignComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::deleteComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::createReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::getReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::getAllReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::saveReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::deleteReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGiverQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGiverQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGiverQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGiverUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGiverUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGiverUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGiverReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGiverReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGiverReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverLastQuestionAnswered" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGiverLastQuestionAnswered"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getLastQuestionAnswered"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverLastQuestionAnswered" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGiverLastQuestionAnswered"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveLastQuestionAnswered"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverLastQuestionAnswered" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGiverLastQuestionAnswered"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteLastQuestionAnswered"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::addAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::assignAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::createReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getAllReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::getReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::addReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::assignReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::deleteReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::createReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::getReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::getAllReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::saveReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::deleteReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceEngineMainQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceEngineMainQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::getMainQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceEngineMainQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceEngineMainQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::saveMainQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceEngineMainQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceEngineMainQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::deleteMainQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::createReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::getReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::getAllReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::saveReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::deleteReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::getResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::addResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::assignResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::deleteResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::createQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::getQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::getAllQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::saveQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::deleteQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::createResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::getResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::getAllResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::saveResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::deleteResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::addQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::assignQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::deleteQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::addQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::assignQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::deleteQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::createQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getAllQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::saveQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::deleteQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::addUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::assignUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::deleteUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "addAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::addReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "assignAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::assignReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::deleteReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::createAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getAllAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::saveAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::deleteAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getActivityUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getActivityUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::getUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveActivityUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveActivityUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::saveUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteActivityUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteActivityUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::deleteUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::createActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::getActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::getAllActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::saveActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::deleteActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getCommentSource" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getCommentSource"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::getSource"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveCommentSource" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveCommentSource"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::saveSource"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteCommentSource" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteCommentSource"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::deleteSource"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::createComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::getComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::getAllComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::saveComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::deleteComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAnswerQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAnswerQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAnswerQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveAnswerQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::saveQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAnswerQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteAnswerQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::deleteQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAnswerResponse" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAnswerResponse"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getResponse"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAnswerResponse" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveAnswerResponse"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::saveResponse"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAnswerResponse" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteAnswerResponse"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::deleteResponse"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::createAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getAllAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::saveAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::deleteAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGroupLinkReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGroupLinkReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGroupLinkReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGroupLinkReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::saveReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGroupLinkReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGroupLinkReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::deleteReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGroupLinkLinkProvider" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGroupLinkLinkProvider"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getLinkProvider"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGroupLinkLinkProvider" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGroupLinkLinkProvider"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::saveLinkProvider"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGroupLinkLinkProvider" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGroupLinkLinkProvider"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::deleteLinkProvider"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "createReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::createReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "getAllReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getAllReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "saveReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::saveReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "deleteReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::deleteReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

